package com.cucumbereng.ebook;

import com.cucumbereng.R;
import com.cucumbereng.util.DebugLog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentPageCredits extends Fragment {
    private static final String TAG = "FragmentPageCredits";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View view = inflater.inflate(R.layout.ebook_f_credits, container, false);
        DebugLog.d(TAG, "onCreateView");
        return view;
    }

}