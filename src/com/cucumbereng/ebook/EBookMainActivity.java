package com.cucumbereng.ebook;

import static com.cucumbereng.ebook.Cheese.PAGE01;
import static com.cucumbereng.ebook.Cheese.PAGE02;
import static com.cucumbereng.ebook.Cheese.PAGE03;
import static com.cucumbereng.ebook.Cheese.PAGE04;
import static com.cucumbereng.ebook.Cheese.PAGE05;
import static com.cucumbereng.ebook.Cheese.PAGE06;
import static com.cucumbereng.ebook.Cheese.PAGE07;
import static com.cucumbereng.ebook.Cheese.PAGE08;
import static com.cucumbereng.ebook.Cheese.PAGECR;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.Toast;

//import com.cucumbereng.KidAndTigerApp;
import com.cucumbereng.KidAndTigerApp;
import com.cucumbereng.MainActivity;
import com.cucumbereng.R;
import com.cucumbereng.util.DebugLog;
import com.cucumbereng.util.EBookUtil;

public class EBookMainActivity extends FragmentActivity implements
		ViewPager.OnPageChangeListener {
	private boolean mMute = false;
	private ViewPager mPager;
	private static final String TAG = "EBookMainActivity";

	private MediaPlayer mSfx;
	private MediaPlayer mSub;
	private Button btn_mute;
	
	/* current fragment position */
	private int mPosition = 0;
	
	private static final String INTENT_START_SETTINGS = "com.cucumbereng.ebook.START_SETTINGS";
	private static final int REQUEST_START_NEW = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ebook_a_main);
		DebugLog.d(TAG, "onCreate");
		
		// add fragments
		List<Fragment> fragments = new ArrayList<Fragment>();
		fragments.add(Fragment.instantiate(this, PAGE01));
		fragments.add(Fragment.instantiate(this, PAGE02));
		fragments.add(Fragment.instantiate(this, PAGE03));
		fragments.add(Fragment.instantiate(this, PAGE04));
		fragments.add(Fragment.instantiate(this, PAGE05));
		fragments.add(Fragment.instantiate(this, PAGE06));
		fragments.add(Fragment.instantiate(this, PAGE07));
		fragments.add(Fragment.instantiate(this, PAGE08));
		fragments.add(Fragment.instantiate(this, PAGECR));

		// adapter
		EBookAdapter adapter = new EBookAdapter(getSupportFragmentManager(),
				fragments);

		// pager
		mPager = (ViewPager) findViewById(R.id.view_pager);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(this);

		playBackground(mPosition);
		playSubtitle(mPosition);
	}

	@Override
	protected void onPause() {
		super.onPause();
		DebugLog.d(TAG, "onPause");

		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
		}

		if (mSub != null) {
			mSub.stop();
			mSub.release();
			mSub = null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		DebugLog.d(TAG, "onResume");

		if (mPosition == 8) {
			playBackground(mPosition);
		} else {
			playBackground(mPosition);
			playSubtitle(mPosition);
		}

//		if (mMute) {
//			mute();
//		} else {
//			unMute();
//		}
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch (requestCode) {
        case REQUEST_START_NEW:
            if (resultCode >= 0 && resultCode <= 9) {
                if (DebugLog.DEBUG_ON) {
                    Toast.makeText(this, "resultCode="+resultCode, Toast.LENGTH_SHORT).show();
                }
                /* setting the viewpager */
                mPager.setCurrentItem(resultCode);
            
            } else if (resultCode == 17) {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
            
            } 
            else if (resultCode == 21) {
//                unMute();
                
            } else if (resultCode == 22) {
//                mute();
            }
            break;

        default:
            break;
        }
    }

	public class EBookAdapter extends FragmentStatePagerAdapter {
		List<Fragment> fragments;

		EBookAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
	}

	@Override
	public void onPageSelected(int position) {
		DebugLog.d(TAG, "fragment position =" + position);

		mPosition = position;

		if (position >= 1 && position <= 7) {
			playBackground(position);
			playSubtitle(position);
			DebugLog.d(TAG, "playing background_sounds=" + position);

		} else if (position == 8) {
			playBackground(position);
			if (mSub != null) {
				mSub.stop();
				mSub.release();
				mSub = null;
			}
			DebugLog.d(TAG, "playing background_sounds=" + position);

		} else {
			playBackground(mPosition);
			playSubtitle(mPosition);
			DebugLog.d(TAG, "playing background_sounds=" + position);
		}

	}

	private void playBackground(int position) {
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();

			mSfx = MediaPlayer.create(this, Cheese.BACKGROUNDS[position]);
			mSfx.start();
			mSfx.setLooping(true);

		} else {
			mSfx = MediaPlayer.create(this, Cheese.BACKGROUNDS[position]);
			mSfx.start();
			mSfx.setLooping(true);
		}

//		if (!mMute) { // not false
//			mute();
//		} else {
//			unMute();
//		}
	}

	private void playSubtitle(int position) {
	    
	    int language = KidAndTigerApp.getInstance().getLanguage(); 
	    switch (language) {
        case EBookUtil.LANG_EN:
            if (mSub != null) {
                mSub.stop();
                mSub.release();

                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_EN[position]);
                mSub.start();
                mSub.setLooping(false);
            } else {
                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_EN[position]);
                mSub.start();
                mSub.setLooping(false);
            }
            break;
            
        case EBookUtil.LANG_MS:
            if (mSub != null) {
                mSub.stop();
                mSub.release();

                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_MS[position]);
                mSub.start();
                mSub.setLooping(false);
            } else {
                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_MS[position]);
                mSub.start();
                mSub.setLooping(false);
            }
            break;
            
        case EBookUtil.LANG_CN:
            if (mSub != null) {
                mSub.stop();
                mSub.release();

                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_CN[position]);
                mSub.start();
                mSub.setLooping(false);
            } else {
                mSub = MediaPlayer.create(this, Cheese.SUBTITLES_CN[position]);
                mSub.start();
                mSub.setLooping(false);
            }
            break;

        }
	    
//		if (!mMute) {
//			mute();
//		} else {
//			unMute();
//		}
	}

	private void mute() {
		mMute = true;

		if (mSfx != null) {
			mSfx.setVolume(0.0f, 0.0f);
		}

		if (mSub != null) {
			mSub.setVolume(0.0f, 0.0f);
		}
		
		btn_mute.setBackgroundResource(R.drawable.sound_mute);
	}

	private void unMute() {
		mMute = false;

		if (mSfx != null) {
			mSfx.setVolume(1.0f, 1.0f);
		}

		if (mSub != null) {
			mSub.setVolume(1.0f, 1.0f);
		}
		
		btn_mute.setBackgroundResource(R.drawable.sounds_style);
	}
}
