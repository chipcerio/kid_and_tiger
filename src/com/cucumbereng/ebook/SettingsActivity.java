package com.cucumbereng.ebook;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.cucumbereng.KidAndTigerApp;
import com.cucumbereng.R;
import com.cucumbereng.util.EBookUtil;

public class SettingsActivity extends Activity implements OnCheckedChangeListener {
    private RadioGroup mRadioGroup;
    // private static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ebook_v3_a_settings);
        mRadioGroup = (RadioGroup) findViewById(R.id.group_v3_languages);
        mRadioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
        case R.id.radio_v3_english:
            KidAndTigerApp.getInstance().setLanguage(EBookUtil.LANG_EN);
            Toast.makeText(this, "English", Toast.LENGTH_SHORT).show();
            break;
            
        case R.id.radio_v3_bahasa:
            KidAndTigerApp.getInstance().setLanguage(EBookUtil.LANG_MS);
            Toast.makeText(this, "Bahasa", Toast.LENGTH_SHORT).show();
            break;
            
        case R.id.radio_v3_simplified_chinese:
            KidAndTigerApp.getInstance().setLanguage(EBookUtil.LANG_CN);
            Toast.makeText(this, "Simplified Chinese", Toast.LENGTH_SHORT).show();
            break;
            
        case R.id.radio_v3_traditional_chinese:
            KidAndTigerApp.getInstance().setLanguage(EBookUtil.LANG_CN);
            Toast.makeText(this, "Traditional Chinese", Toast.LENGTH_SHORT).show();
            break;
            
        }
    }
    
    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}