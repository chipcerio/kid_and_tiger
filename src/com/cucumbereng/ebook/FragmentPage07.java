package com.cucumbereng.ebook;

import com.cucumbereng.R;
import com.cucumbereng.util.DebugLog;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentPage07 extends Fragment {
    private Handler mHandler;
    private TextView mText;
    private static final String TAG = "FragmentPage07";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View view = inflater.inflate(R.layout.ebook_f_page07, container, false);
        DebugLog.d(TAG, "onCreateView");
        
        // initialise
        mText = (TextView) view.findViewById(R.id.narration_page7);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        setUserVisibleHint(false);
        
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        DebugLog.d(TAG, "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        DebugLog.d(TAG, "onResume");
        
        if (mVisible) {
            play(R.string.page07_01);
        } else {
            stop();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        DebugLog.d(TAG, "onStart");
        if (mHandler == null) {
            mHandler = new Handler();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        DebugLog.d(TAG, "onStop");
        if (mHandler != null) {
            mHandler = null;
            DebugLog.d(TAG, "mHandler=null");
        }
        stop();
    }
    
    private boolean mVisible = false;
    
    @Override
    public void setMenuVisibility(boolean visible) {
        super.setMenuVisibility(visible);
        DebugLog.d(TAG, "setMenuVisibility=" + visible);
        
        if (visible) {
            play(R.string.page07_01);
        } else {
            stop();
        }
        mVisible = visible;
    }
    
    private void play(int resId) {
        if (mHandler != null) {
            mHandler.post(mRun);
            mInterval = 0;
            mText.setText(resId);
        }
    }
    
    private void stop() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRun);
            mInterval = 0;
        }
    }
    
    private int mInterval = 0;
    private Runnable mRun = new Runnable() {

        @Override
        public void run() {
            if (mInterval == 8) {
                mText.setText(R.string.page07_02);
            } else if (mInterval == 11) {
                mText.setText(R.string.page07_03);
            }

            DebugLog.d(TAG, "count=" + mInterval++);
            if (mHandler != null) {
                mHandler.postDelayed(this, 1000);
            }
        }
    };

}