package com.cucumbereng.ebook;

import com.cucumbereng.R;

/* static class */
public class Cheese {
    
    // do not re-arrange the values of the indices
    public static final int[] BACKGROUNDS = {
        R.raw.sfx01,
        R.raw.sfx02,
        R.raw.sfx03,
        R.raw.sfx04,
        R.raw.sfx05,
        R.raw.sfx06,
        R.raw.sfx07,
        R.raw.sfx08,
        R.raw.sfx00
    };
    
    // do not re-arrange the values of the indices
    public static final int[] SUBTITLES_EN = {
        R.raw.page1,
        R.raw.page2,
        R.raw.page3,
        R.raw.page4,
        R.raw.page5,
        R.raw.page6,
        R.raw.page7,
        R.raw.page8
    };
    
    // do not re-arrange the values of the indices
    public static final int[] SUBTITLES_CN = {
        R.raw.pg1_cn,
        R.raw.pg2_cn,
        R.raw.pg3_cn,
        R.raw.pg4_cn,
        R.raw.pg5_cn,
        R.raw.pg6_cn,
        R.raw.pg7_cn,
        R.raw.pg8_cn
    };
    
    // do not re-arrange the values of the indices
    public static final int[] SUBTITLES_MS = {
        R.raw.pg1_ms,
        R.raw.pg2_ms,
        R.raw.pg3_ms,
        R.raw.pg4_ms,
        R.raw.pg5_ms,
        R.raw.pg6_ms,
        R.raw.pg7_ms,
        R.raw.pg8_ms
    };
    
    public static final int[] DRAWABLES = {
        R.drawable.thumb_ebook_bg01,
        R.drawable.thumb_ebook_bg02,
        R.drawable.thumb_ebook_bg03,
        R.drawable.thumb_ebook_bg04,
        R.drawable.thumb_ebook_bg05,
        R.drawable.thumb_ebook_bg06,
        R.drawable.thumb_ebook_bg07,
        R.drawable.thumb_ebook_bg08
    };
    
    public static final String PAGE01 = FragmentPage01.class.getName();
    public static final String PAGE02 = FragmentPage02.class.getName();
    public static final String PAGE03 = FragmentPage03.class.getName();
    public static final String PAGE04 = FragmentPage04.class.getName();
    public static final String PAGE05 = FragmentPage05.class.getName();
    public static final String PAGE06 = FragmentPage06.class.getName();
    public static final String PAGE07 = FragmentPage07.class.getName();
    public static final String PAGE08 = FragmentPage08.class.getName();
    public static final String PAGECR = FragmentPageCredits.class.getName();
    
}
