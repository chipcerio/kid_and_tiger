package com.cucumbereng;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cucumbereng.ebook.EBookMainActivity;
import com.cucumbereng.ebook.SettingsActivity;
import com.cucumbereng.util.EBookUtil;

public class MainActivity extends Activity implements View.OnClickListener {
    private int mLanguage;
    private ImageButton mReadMore;
    private ImageButton mSettings;
    private static final int READ_MORE_CLICK = 1;
    private static final int SETTINGS_CLICK = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLanguage = KidAndTigerApp.getInstance().getLanguage();
        setLayout();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        mLanguage = KidAndTigerApp.getInstance().getLanguage();
        displayMultiLanguageButtons();
    }
    
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setLayout() {
        RelativeLayout rlayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams rparams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        rlayout.setBackground(getResources().getDrawable(R.drawable.bg06));
        rlayout.setLayoutParams(rparams);
        
        // 1st image button
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params1.setMargins(0, 0, 0, 40);
        mReadMore = new ImageButton(this);
        mReadMore.setId(READ_MORE_CLICK);
        mReadMore.setLayoutParams(params1);
        rlayout.addView(mReadMore);
        
        // 2nd image button
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params2.addRule(RelativeLayout.ABOVE, 1);
        mSettings = new ImageButton(this);
        mSettings.setId(SETTINGS_CLICK);
        mSettings.setLayoutParams(params2);
        rlayout.addView(mSettings);
        
        // display multi-language buttons
        displayMultiLanguageButtons();
        
        // layout
        setContentView(rlayout);
        
        // listeners
        mReadMore.setOnClickListener(this);
        mSettings.setOnClickListener(this);
    }
    
    private void displayMultiLanguageButtons() {
        mLanguage = KidAndTigerApp.getInstance().getLanguage();
        switch (mLanguage) {
        case EBookUtil.LANG_EN:
            mReadMore.setImageDrawable(getResources().getDrawable(R.drawable.cover_read2me_en));
            mSettings.setImageDrawable(getResources().getDrawable(R.drawable.cover_setting_en));
            break;
            
        case EBookUtil.LANG_MS:
            mReadMore.setImageDrawable(getResources().getDrawable(R.drawable.cover_read2me_ms));
            mSettings.setImageDrawable(getResources().getDrawable(R.drawable.cover_setting_ms));
            break;
            
        case EBookUtil.LANG_CN:
            mReadMore.setImageDrawable(getResources().getDrawable(R.drawable.cover_read2me_cn));
            mSettings.setImageDrawable(getResources().getDrawable(R.drawable.cover_setting_cn));
            break;
            
        case EBookUtil.LANG_ZH:
            mReadMore.setImageDrawable(getResources().getDrawable(R.drawable.cover_read2me_zh));
            mSettings.setImageDrawable(getResources().getDrawable(R.drawable.cover_setting_zh));
            break;

        default:
            break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case READ_MORE_CLICK:
            Intent j = new Intent(this, EBookMainActivity.class);
            startActivity(j);
            break;
            
        case SETTINGS_CLICK:
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            break;

        }
    }

}
