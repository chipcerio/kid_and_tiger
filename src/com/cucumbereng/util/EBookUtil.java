package com.cucumbereng.util;

public class EBookUtil {

    public static final int LANG_EN = 1;
    public static final int LANG_MS = 2;
    public static final int LANG_CN = 3;
    public static final int LANG_ZH = 4;
}
