package com.cucumbereng;

import android.app.Application;
import android.content.SharedPreferences;

public class KidAndTigerApp extends Application {
    private static KidAndTigerApp mInstance;
    private static SharedPreferences mPref;
    
    private static final String USER_PREF = "user_pref";
    private static final String SFX_PREF = "bg_pref";
    private static final String LANG_PREF = "language";
    
    public KidAndTigerApp() {
        super();
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
    
    public static KidAndTigerApp getInstance() {
        if (mInstance == null) {
            synchronized (KidAndTigerApp.class) {
                if (mInstance == null) {
                    new KidAndTigerApp();
                }
            }
        }
        
        if (mPref == null) {
            mPref = mInstance.getSharedPreferences(USER_PREF, MODE_PRIVATE);
        }
        
        return mInstance;
    }
    
    public void setSfx(boolean isMute) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(SFX_PREF, isMute);
        editor.commit();
    }
    
    public boolean getSfx() {
        return mPref.getBoolean(SFX_PREF, true);
    }
    
    public void setLanguage(int language) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putInt(LANG_PREF, language);
        editor.commit();
    }
    
    public int getLanguage() {
        return mPref.getInt(LANG_PREF, 1);
    }
    
}
